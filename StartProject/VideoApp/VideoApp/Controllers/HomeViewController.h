//
//  HomeViewController.h
//  VideoApp
//
//  Created by Dan Bodnar on 16/04/15.
//  Copyright (c) 2015 Dan Bodnar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface HomeViewController : RootViewController

@end
