//
//  AppDelegate.h
//  VideoApp
//
//  Created by Dan Bodnar on 16/04/15.
//  Copyright (c) 2015 Dan Bodnar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

