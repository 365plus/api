//
//  Config.h
//  App
//
//  Created by Dan Bodnar on 12/1/14.
//  Copyright (c) 2014 Dan B. All rights reserved.
//


#define appDelegate                 ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define DEBUG_MODE                  NO

#define IsProVersion                [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"IsProVersion"] intValue]
#define AdMobInterstitial           [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AdMobInterstitial"]
#define AdMobBanner                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AdMobBanner"]

#define AppStoreId                  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppStoreId"]
#define FeedbackMail                [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FeedbackMail"]

#define AppCategory                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppCategory"]
