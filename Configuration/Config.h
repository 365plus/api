//
//  Config.h
//  App
//
//  Created by Dan Bodnar on 12/1/14.
//  Copyright (c) 2014 Dan B. All rights reserved.
//


#define appDelegate                 ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define DEBUG_MODE                  NO

#define IsProVersion                [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"IsProVersion"] intValue]
#define AdMobInterstitial           [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AdMobInterstitial"]
#define AdMobBanner                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AdMobBanner"]

#define AppStoreId                  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppStoreId"]
#define FeedbackMail                [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FeedbackMail"]

#define AppCategory                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppCategory"]

#define IsMovieApp                  [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"IsMovieApp"] intValue]
#define NumberOfPositions           [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"NumberOfPositions"] intValue]
#define DocountdownTime             [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DocountdownTime"] intValue]

#define FONT_NAME                   @"api"

#define TITLE_1                     @"Workout Setup - 1"
#define DESC_1                      @"This is a simple way to better see and understand the selected spinning workout. In our PLUS version, here you can setup your own workouts (up to 6 different programs). Design them from scratch or modify the default workout to reach your demands."

#define TITLE_2                     @"Workout Setup - 2"
#define DESC_2                      @"Each row is a step in the program (the step number is written in a gray square on each row). In PLUS version, choose position, time, resistance and cadence for each step in your program. Don't worry, you can modify it later"

#define TITLE_3                     @"Music Setup"
#define DESC_3                      @"The training is better with music. Load your tracks from your Music Application. We have suggested you some songs for the Default Workout, but you can select any tracks you wish. You can also choose the step  when the track will play."

#define TITLE_4                     @"Workout - Timing"
#define DESC_4                      @"On top of the screen you can see the workout time: current step Countdown (seconds until the current step finishes) and the total Workout Time"

#define TITLE_5                     @"Workout - Positions"
#define DESC_5                      @"As the name indicates, the current position represents the position you need be in (tap once on the silhouettes to see them better). Next position is the position you should take after to Countdown reaches 0"

#define TITLE_6                     @"Workout - Resistance"
#define DESC_6                      @"This is the resistance configuration of your bike. You can adapt this based on your experience (beginners should have around 10 to 20 points less)"

#define TITLE_7                     @"Workout - Cadence"
#define DESC_7                      @"The spinning wheel is a graphic representation of your pedals. The RED dot is the RIGHT pedal and the GRAY one is the LEFT one. "