//
//  GenericObjectCache.h
//  cloudserverapp
//
//  Created by Dragos Nita on 5/13/12.
//  Copyright (c) 2012 Seesmic. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////

//
// a class that stores the cached objects
//
@interface GenericObjectCache : NSObject <NSObject>
{
	id object;
	NSString *name;
	NSTimeInterval expirationInterval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

@property (readonly) NSString *name;
@property (assign) NSTimeInterval expirationInterval;

////////////////////////////////////////////////////////////////////////////////////////////////////

// inits a cache for a specified object name
-(id)initWithName:(NSString*)name expirationInterval:(NSTimeInterval)expInt;

// returns a cached object
-(id)getObject;

// set the object to be cached
-(void)setObject:(id)obj;

//returns true if the cache is valid (recent enough)
-(bool)isValid;

// invalidate a specified object cached
+(void)invalidate:(NSString*)name;

+(NSString*)getCurrentDirectory;

////////////////////////////////////////////////////////////////////////////////////////////////////

@end

////////////////////////////////////////////////////////////////////////////////////////////////////
