//
//  GenericObjectCache.m
//  cloudserverapp
//
//  Created by Dragos Nita on 5/13/12.
//  Copyright (c) 2012 Seesmic. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////

#import "GenericObjectCache.h"
#import "Reachability.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

static NSMutableDictionary *generalCache = nil;

////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation GenericObjectCache

////////////////////////////////////////////////////////////////////////////////////////////////////

@synthesize name, expirationInterval;

////////////////////////////////////////////////////////////////////////////////////////////////////

+(NSString*)getCurrentDirectory
{
	static NSString* currentDirectory = nil;
    
	if(currentDirectory)
		return currentDirectory;
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	currentDirectory = [paths objectAtIndex:0];

	
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"currentDirectory : %@", currentDirectory);
#endif
	
	return currentDirectory;
}

- (NSString*)createFilename
{
	return [NSString stringWithFormat:@"%@/generalCache.list", [GenericObjectCache getCurrentDirectory]];
}

////////////////////////////////////////////////////////////////////////////////////////////////////

-(NSString*)getLastUpdatedSaveTag
{
    return [NSString stringWithFormat:@"last_updated_%@", name];
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//
// inits a cache for a specified object name
//
-(id)initWithName:(NSString*)_name expirationInterval:(NSTimeInterval)expInt
{
    self = [super init];
	if(self)
	{
		object = nil;
        expirationInterval = expInt;
		name = _name;
        
		static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			
			NSData *encryptedData = [NSData dataWithContentsOfFile:[self createFilename]];
            id obj = nil;
            
            if(encryptedData)
                obj = [NSKeyedUnarchiver unarchiveObjectWithData:encryptedData];
			
			if(obj)
				generalCache = [NSMutableDictionary dictionaryWithDictionary:obj];
			else
			{
				generalCache = [[NSMutableDictionary alloc] init];
				[generalCache setObject:[NSMutableDictionary dictionary] forKey:@"data_update_time"];
			}
		});

        object = [generalCache objectForKey:name];
	}
	
	return self;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//
// returns a cached object
//
-(id)getObject
{
//	NSDate *lastUpdated = [[generalCache objectForKey:@"data_update_time"] objectForKey:[self getLastUpdatedSaveTag]];
//	NSTimeInterval timeInterval = -[lastUpdated timeIntervalSinceNow];
//
//	if(timeInterval > expirationInterval)
//		return nil;

	return object;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//
// set the object to be cached
//
-(void)setObject:(id)obj
{
	object = obj;
	
	@synchronized(self)
	{
		if(object)
		{
			[generalCache setObject:object forKey:name];
			[[generalCache objectForKey:@"data_update_time"] setObject:[NSDate date] forKey:[self getLastUpdatedSaveTag]];
		}
		else
		{
			[generalCache removeObjectForKey:name];
			[[generalCache objectForKey:@"data_update_time"] removeObjectForKey:[self getLastUpdatedSaveTag]];
		}
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject:generalCache];
		[data writeToFile:[self createFilename] atomically:YES];
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//returns true if the cache is valid (recent enough)
-(bool)isValid
{
    Reachability *r = [Reachability reachabilityForInternetConnection];
    if(!r.isReachable)
        return YES;

	NSDate *lastUpdated = [[generalCache objectForKey:@"data_update_time"] objectForKey:[self getLastUpdatedSaveTag]];
	NSTimeInterval timeInterval = -[lastUpdated timeIntervalSinceNow];
	
	return ((timeInterval < expirationInterval) && (object != nil));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

// invalidate a specified object cached
+(void)invalidate:(NSString*)name
{
    Reachability *r = [Reachability reachabilityForInternetConnection];
    if(!r.isReachable)
        return;

	GenericObjectCache *cache = [[GenericObjectCache alloc] initWithName:name expirationInterval:1];
	
	if(name)
	{
		[cache setObject:nil];
//		[generalCache removeObjectForKey:name];
//		[[generalCache objectForKey:@"data_update_time"] removeObjectForKey:[NSString stringWithFormat:@"last_updated_%@", name]];
	}
	else
	{
		[generalCache removeAllObjects];
		[generalCache setObject:[NSMutableDictionary dictionary] forKey:@"data_update_time"];
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////

@end

////////////////////////////////////////////////////////////////////////////////////////////////////
