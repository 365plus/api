//
//  UILabelFont.m
//  RegApp
//
//  Created by Dan Bodnar on 03/08/15.
//  Copyright (c) 2015 Dan Bodnar. All rights reserved.
//

#import "UILabelFont.h"
#import "Config.h"

@implementation UILabelFont


- (void)awakeFromNib
{
    UIFont *font = [UIFont fontWithName:FONT_NAME size:self.font.pointSize];
    
    if([self respondsToSelector:@selector(fontName)] && [self valueForKey:@"fontName"]!=nil)
        font = [UIFont fontWithName:[self valueForKey:@"fontName"] size:self.font.pointSize];
    
    self.font = font;
}

@end
