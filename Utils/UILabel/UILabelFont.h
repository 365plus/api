//
//  UILabelFont.h
//  RegApp
//
//  Created by Dan Bodnar on 03/08/15.
//  Copyright (c) 2015 Dan Bodnar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabelFont : UILabel

@property (strong) NSString *fontName;

@end
