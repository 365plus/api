//
//  UIButton+Font.m
//  VideoApp
//
//  Created by Dan Bodnar on 09/07/15.
//  Copyright (c) 2015 Dan Bodnar. All rights reserved.
//

#import "UIButtonFont.h"
#import "Config.h"

@implementation UIButtonFont


- (void)awakeFromNib
{
    UIFont *font = [UIFont fontWithName:FONT_NAME size:self.titleLabel.font.pointSize];
    
    if([self respondsToSelector:@selector(fontName)] && [self valueForKey:@"fontName"]!=nil)
        font = [UIFont fontWithName:[self valueForKey:@"fontName"] size:self.titleLabel.font.pointSize];
    
    self.titleLabel.font = font;
}


@end
