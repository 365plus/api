//
//  UIButton+Font.h
//  VideoApp
//
//  Created by Dan Bodnar on 09/07/15.
//  Copyright (c) 2015 Dan Bodnar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButtonFont : UIButton

@property (strong) NSString *fontName;

@end
