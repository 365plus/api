//
//  MoviePlayer.m
//  Slideshow
//
//  Created by Toto on 2/20/14.
//  Copyright (c) 2014 365plus. All rights reserved.
//

#import "MoviePlayer.h"
#import <AVFoundation/AVFoundation.h>

@implementation MoviePlayer


+ (Class)layerClass
{
    return [AVPlayerLayer class];
}

- (AVPlayer*)player
{
    return [(AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVPlayer *)player
{
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

@end

