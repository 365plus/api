//
//  MoviePlayer.h
//  Slideshow
//
//  Created by Toto on 2/20/14.
//  Copyright (c) 2014 365plus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVPlayer.h>

@interface MoviePlayer : UIView

@property (nonatomic, retain) AVPlayer* player;

+ (Class)layerClass;
- (void)setPlayer:(AVPlayer*)player;

@end
