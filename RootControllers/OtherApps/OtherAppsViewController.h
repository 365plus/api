//
//  OtherAppsViewController.h
//  MemeCreator
//
//  Created by Alexandru Paduraru on 7/4/14.
//  Copyright (c) 2014 Alexandru Paduraru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface OtherAppsViewController : UIViewController <SKStoreProductViewControllerDelegate>
{
    NSMutableArray *appsArray;
    NSMutableArray *keys;
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIActivityIndicatorView *loading;
    IBOutlet UIButton *closeBtn;
}

- (IBAction)close:(id)sender;

@end
