//
//  ZzApp.h
//  MemeCreator
//
//  Created by Alexandru Paduraru on 7/4/14.
//  Copyright (c) 2014 Alexandru Paduraru. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZzApp : NSObject

@property (nonatomic, strong) NSString *logo;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSString *itunesId;
@property (nonatomic, strong) NSString *category;

@end