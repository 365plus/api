//
//  IconsCell
//
//  Created by Alexandru Paduraru on 3/10/12.
//

#import <UIKit/UIKit.h>

@interface AppCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) IBOutlet UILabel *subtitle;

@end


