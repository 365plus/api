//
//  OtherAppsViewController.m
//  MemeCreator
//
//  Created by Alexandru Paduraru on 7/4/14.
//  Copyright (c) 2014 Alexandru Paduraru. All rights reserved.
//

#import "OtherAppsViewController.h"
#import "AppCell.h"
#import "ZzApp.h"

#import "Config.h"
#import "UIImageView+WebCache.h"

@import GoogleMobileAds;


// This category (i.e. class extension) is a workaround to get the
// Image PickerController to appear in landscape mode.
@interface SKStoreProductViewController(Nonrotating)
- (BOOL)shouldAutorotate;
@end

@implementation SKStoreProductViewController(Nonrotating)

- (BOOL)shouldAutorotate {
    return NO;
}
@end


@implementation OtherAppsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    closeBtn.titleLabel.font = [UIFont fontWithName:@"api" size:20];
    
    //NSString* stringsFile = @"settings";//[NSString stringWithFormat:@"settings_%@", strings_lang];
    //titleLabel.text =  NSLocalizedStringFromTable(@"OtherAppsScreen.Title", stringsFile, @"nil");
    
    tableView.hidden  = YES;
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^(void){
                       
                       NSString *urlString = [NSString stringWithFormat:@"http://service.365plus.net/ajax/get-apps/%d", IsProVersion];
                       NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                       NSError *error;
                       NSArray *array = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
                       
                       appsArray = [NSMutableArray new];
                       keys = [NSMutableArray new];
                       if (array && [array isKindOfClass:[NSArray class]])
                       {
                           for (NSDictionary *dict in array)
                           {
                               //add object - except current
                               if (![[dict objectForKey:@"itunesId"] isEqualToString:AppStoreId])
                               {
                                   ZzApp *app = [ZzApp new];
                                   app.title = [dict objectForKey:@"title"];
                                   app.logo = [@"http://service.365plus.net/assets/" stringByAppendingString:[dict objectForKey:@"logo"]];
                                   app.descriptionText = [dict objectForKey:@"description"];
                                   app.itunesId = [dict objectForKey:@"itunesId"];
                                   app.category = [dict objectForKey:@"category"];
                                   
                                   //add it, except the current one
                                   [appsArray addObject:app];
                                   
                                   //add its category
                                   if (![keys containsObject:app.category])
                                   {
                                       [keys addObject:app.category];
                                   }
                               }
                           }
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               //move this category to up
                               if ([keys containsObject:AppCategory])
                               {
                                   [keys removeObject:AppCategory];
                                   [keys insertObject:AppCategory atIndex:0];
                               }
                               
                               [tableView reloadData];
                               tableView.hidden = NO;
                               [loading stopAnimating];
                           });
                       }
                   });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return keys.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if([keys[section] isEqualToString:AppCategory])
        return @"Related";
    return [[keys[section] uppercaseString] stringByAppendingString:@""];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = keys[section];
    NSArray *categoryArray = [appsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"category = %@", key]];
    
    if(IsProVersion)
        return categoryArray.count + 1;
    else
        return categoryArray.count;
}

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = keys[indexPath.section];
    NSArray *categoryArray = [appsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"category = %@", key]];
    
    if (indexPath.row == categoryArray.count)
        return kGADAdSizeBanner.size.height;
    
    return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *key = keys[indexPath.section];
    NSArray *categoryArray = [appsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"category = %@", key]];
    
    //ad
    if (indexPath.row == categoryArray.count)
    {
        NSString* cellIdentifier = @"AdCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        bannerView.adUnitID = AdMobBanner;
        bannerView.rootViewController = self;
        [cell.contentView addSubview:bannerView];
        [bannerView loadRequest:[GADRequest request]];
        
        cell.backgroundColor = [UIColor clearColor];
        bannerView.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
    
    NSString* cellIdentifier = @"AppCell";
    AppCell* cell = (AppCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = (AppCell*) [[[NSBundle mainBundle] loadNibNamed:@"AppCell" owner:self options:nil] objectAtIndex:0];
    }
    
    ZzApp *app = [categoryArray objectAtIndex:indexPath.row];
    [cell.logo sd_setImageWithURL:[NSURL URLWithString:app.logo]];
    cell.title.text = app.title;
    cell.subtitle.text = app.descriptionText;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = keys[indexPath.section];
    NSArray *categoryArray = [appsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"category = %@", key]];
    ZzApp *app = [categoryArray objectAtIndex:indexPath.row];
    
    // Initialize Product View Controller
    SKStoreProductViewController *storeProductViewController = [[SKStoreProductViewController alloc] init];
    [storeProductViewController setDelegate:self];
    
    [storeProductViewController loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier : app.itunesId} completionBlock:^(BOOL result, NSError *error)
     {
         [self presentViewController:storeProductViewController animated:YES completion:nil];
     }];

}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
