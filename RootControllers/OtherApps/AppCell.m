//
//  StoreCell.h
//  BetApp
//
//  Created by Alexandru Paduraru on 3/10/12.
//

#import "AppCell.h"

@implementation AppCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.logo.layer.masksToBounds = YES;
    self.logo.layer.cornerRadius = 8.0f;
}

@end
