//
//  BaseViewController.h
//  VideoApp
//
//  Created by Dan Bodnar on 12/06/15.
//  Copyright (c) 2015 Dan Bodnar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface BaseViewController : UIViewController <MBProgressHUDDelegate>

@property (strong, nonatomic) MBProgressHUD *HUD;

@end
