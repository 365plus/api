//
//  RootViewController.m
//  Spinning
//
//  Created by Dan Bodnar on 12/24/14.
//  Copyright (c) 2014 Dan B. All rights reserved.
//

#import "RootViewController.h"
#import "OtherAppsViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController
{
    GADBannerView *adMobView;
    int screenHeight;
}

@synthesize adView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(!IsProVersion && UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft ||
           [UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight)
            adMobView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerLandscape];
        else
            adMobView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        
        adMobView.adUnitID = AdMobBanner;
        adMobView.delegate = self;
        adMobView.rootViewController = self;
        
        if(!adView)
        {
            if([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight)
                adView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, kGADAdSizeSmartBannerLandscape.size.height)];
            else
                adView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, kGADAdSizeSmartBannerPortrait.size.height)];
            
            [self.view addSubview:adView];
        }
    }
    
    screenHeight = self.view.frame.size.height;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 32, 32);
    [button addTarget:self action:@selector(showOptions) forControlEvents:UIControlEventTouchUpInside];
    button.titleLabel.font = [UIFont fontWithName:@"api" size:25];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [button setTitle:@"" forState:UIControlStateNormal];
    
    [self.view addSubview:button];
    UIBarButtonItem *barButton=[[UIBarButtonItem alloc] init];
    [barButton setCustomView:button];
    self.navigationItem.rightBarButtonItem=barButton;
}

-(void)viewDidAppear:(BOOL)animated
{
    if(!IsProVersion)
    {
        [adMobView loadRequest:[GADRequest request]];
        [adView addSubview:adMobView];
    }
}

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    if(item.tag == 1)
    {
        switch (itemIndex) {
            case 0:
                [self rateApp];
                break;
            case 1:
                [self sendEmail];
                break;
            case 2:
                [self otherApps];
                break;
            case 3:
                [self showInfo];
                break;
            default:
                break;
        }
    }
}

-(void)showOptions
{
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithTitle:@"" andSubtitle:@"Rate app"],
                       [[RNGridMenuItem alloc] initWithTitle:@"" andSubtitle:@"Feedback"],
                       [[RNGridMenuItem alloc] initWithTitle:@"" andSubtitle:@"Other apps"],
                       [[RNGridMenuItem alloc] initWithTitle:@"" andSubtitle:@"Info"],
                       ];
    
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, [items count])]];
    av.delegate = self;
    //av.bounces = NO;
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

-(void)otherApps
{
    OtherAppsViewController *controller = [OtherAppsViewController new];
    [self presentViewController:controller animated:NO completion:nil];
}

-(void)rateApp
{
    NSString *templateReviewURL = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=APP_ID&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";
    NSString *reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%@", AppStoreId]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
}

-(void)sendEmail
{
    if([MFMailComposeViewController canSendMail])
    {
        //Create the mail composer window
        MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
        emailDialog.mailComposeDelegate = self;
        
        NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
        NSString *mailSubject = [NSString stringWithFormat:@"%@ %@ v. %@", @"Feedback for ", [info objectForKey:@"CFBundleDisplayName"], [info objectForKey:@"CFBundleVersion"]];
        [emailDialog setSubject:mailSubject];
        [emailDialog setToRecipients:@[FeedbackMail]];
        
        [self.parentViewController presentViewController:emailDialog animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                       message:@"You need to have an email account configured on this device. Go to Configuration and add an email account."
                                                      delegate:nil
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
    }
}

-(void)showInfo {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                   message:@"This application is not a medical advice. It is intended for general informational purposes only and does not address individual circumstances.\n\r The advice and information contained on this application may not be appropriate for all individuals.\n\r You should consult a physician before starting any diet or exercise program.\n\rIf you feel any pain and/or discomfort, STOP the exercise and consult a physician."
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
    [alert show];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)adViewDidReceiveAd:(GADBannerView *)adMob {
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        adView.frame = CGRectMake(0, screenHeight - adMobView.frame.size.height, self.view.frame.size.width, adMobView.frame.size.width);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
