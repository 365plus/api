//
//  RootViewController.h
//  Spinning
//
//  Created by Dan Bodnar on 12/24/14.
//  Copyright (c) 2014 Dan B. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MessageUI/MFMailComposeViewController.h>

#import "Config.h"

#import "RNGridMenu.h"
#import "Application.h"

@import GoogleMobileAds;

@interface RootViewController : UIViewController <GADBannerViewDelegate, RNGridMenuDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UIView *adView;

@end
