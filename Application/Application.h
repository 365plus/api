//
//  Application.h
//  Flipaview
//
//  Created by Dan Bodnar on 8/12/14.
//  Copyright (c) 2014 365plus. All rights reserved.
//

#import <Foundation/Foundation.h>

@import GoogleMobileAds;

#import "Config.h"

//from apis_ios/Common/Utils/Application.h

@interface Application : NSObject <GADInterstitialDelegate>
{
    void (^adDismissBlock)(void);
    GADInterstitial *interstitial;
}

@property (nonatomic, strong) UIStoryboard *storyboard;

+ (Application *)sharedApplication;

- (void)loadFullAd;
- (void)tryToShowAdForEvent:(NSString *)eventName at:(int)count;
- (void)showFullAdOnOpen:(void (^)(void))dismissBlock;

@end
