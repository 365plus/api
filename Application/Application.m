//
//  Application.m
//  Flipaview
//
//  Created by Dan Bodnar on 8/12/14.
//  Copyright (c) 2014 365plus. All rights reserved.
//

#import "Application.h"
#import "AppDelegate.h"

@implementation Application

+ (Application *)sharedApplication
{
    static Application *obj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
		obj = [[Application alloc] init];
    });
	
    return obj;
}

- (id)init
{
    self = [super init];
    
    if(self)
    {
        [self setStoryboardBasedOnDevice];
    }
    
    return self;
}

-(void)setStoryboardBasedOnDevice
{
    //if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    //    self.storyboard = [UIStoryboard storyboardWithName:@"MainiPad" bundle:nil];
    //else
    self.storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
}


- (void)loadFullAd
{
    if (IsProVersion != 1 && (!interstitial || interstitial.hasBeenUsed))
    {
        interstitial = [[GADInterstitial alloc] initWithAdUnitID:AdMobInterstitial];
        interstitial.delegate = self;
        
        GADRequest *request = [GADRequest request];
        [interstitial loadRequest:request];
        adDismissBlock = nil;
    }
}

//show the ad - only if ready to be shown
- (void)showFullAdOnOpen:(void (^)(void))dismissBlock
{
    if (IsProVersion != 1 && interstitial.isReady)
    {
        [interstitial presentFromRootViewController:((AppDelegate*)[[UIApplication sharedApplication] delegate]).window.rootViewController];
        
        adDismissBlock = dismissBlock;
        //interstitial = nil;
    }
    else
    {
        if (dismissBlock) {
            dismissBlock();
        }
    }
}

//called on dismiss ad - if any close bloc, call it
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad
{
    if (adDismissBlock)
    {
        adDismissBlock();
        adDismissBlock = nil;
    }
    
    [self loadFullAd];
}

- (void)tryToShowAdForEvent:(NSString *)eventName at:(int)count
{
    if (IsProVersion != 1 && interstitial.isReady)
    {
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        
        //increate event count
        NSInteger enterNb = [def integerForKey:eventName];
        enterNb++;
        [def setInteger:enterNb forKey:eventName];
        [def synchronize];
        
        //show it
        if (count && (enterNb % count == 0))
            [[Application sharedApplication] showFullAdOnOpen:nil];
    }
}


@end
